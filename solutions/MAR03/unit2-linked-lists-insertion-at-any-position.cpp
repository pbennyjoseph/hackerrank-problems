#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#define new znew
#define LinkedListNode Node
using namespace std;
typedef struct Node{
    int val;
    struct Node * next;
} Node ;
Node * insert(Node * head,int val,int pos){
    LinkedListNode * new = (LinkedListNode *) malloc(sizeof(LinkedListNode));
    new->next = NULL;
    new->val = val;
    if(!head) return new;
    if(pos==1){
        new->next = head;
        return new;
    }
    LinkedListNode * ref = head;
    --pos;
    while(--pos && ref->next){
        ref = ref->next;
    }
    if(pos) return head;
    new->next = ref->next;
    ref->next = new;
    return head;
}

void print(Node * head){
    if(!head) return ;
    if(head->next){
        printf("%d->",head->val);
    }
    else printf("%d\n",head->val);
    print(head->next);
}

int main() {
    int n;
    Node * head = NULL;
    cin >> n;
    while(n--){
        int x,pos;
        cin >> pos >> x;
        head = insert(head,x,++pos);
    }
    print(head);
    return 0;
}
