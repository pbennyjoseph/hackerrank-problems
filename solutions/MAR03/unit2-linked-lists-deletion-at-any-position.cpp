#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#define new znew
#define LinkedListNode Node
#define delete deleter
using namespace std;
typedef struct Node{
    int val;
    struct Node * next;
} Node ;
LinkedListNode* insert(LinkedListNode* head, int val) {
    LinkedListNode * new = (LinkedListNode *) malloc(sizeof(LinkedListNode));
    new->next = NULL;
    new->val = val;
    if(!head) return new;
    LinkedListNode * ref = head;
    while(head->next){
        head = head->next;
    }
    head->next = new;
    return ref;
}
LinkedListNode* deleter(LinkedListNode* head, int pos) {
    if(!head) return head;
    if(!head->next) return NULL;
    LinkedListNode * ref = head;
    if(pos==1){
        head = head->next;
        return head;
    }
    --pos;
    while(--pos && head->next && head->next->next)
        head = head->next;
    LinkedListNode * temp = head->next;
    if(pos) return ref;
    head->next = head->next->next;
    return ref;
}


void print(Node * head){
    if(!head) return ;
    if(head->next){
        printf("%d->",head->val);
    }
    else printf("%d\n",head->val);
    print(head->next);
}

int main() {
    int n;
    Node * head = NULL;
    cin >> n;
    while(n--){
        int x,pos;
        cin >> x;
        head = insert(head,x);
    }
    int k;
    cin >> k;
    head = delete(head,++k);
    print(head);
    return 0;
}
