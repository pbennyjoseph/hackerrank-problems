#include <bits/stdc++.h>
using namespace std;


int main() {
    int t;
    cin >> t;
    unordered_map<char,char> uu;
    uu['>'] = '<';
    uu['}'] = '{';
    uu[')'] = '(';
    
    while(t--){
        string s;
        cin >> s;
        stack<char> m;
        int instrcount = 0;
        int anklecount = 0;
        bool insideUserFun=false,inmain = false,mainProg=false;
        if(s[0]=='{' && s[s.length()-1]=='}') s = s.substr(1,s.length()-2);
        // m.push('{');
        
        // s = s.substr(1,s.length()-1);
        for(auto&x: s){
            if(x=='>' || x=='}' || x==')'){
                if(m.empty() || m.top() != uu[x]) goto err;
                else{
                    if(x=='>')
                        inmain = false;
                    else if(x==')'){
                        insideUserFun = false;
                        instrcount = 0;
                    }
                    m.pop();
                }
            }
            else if(x=='<' || x=='{' || x=='('){
                if(x=='<'){
                    anklecount++;
                    inmain = true;
                }
                if((insideUserFun || inmain) && x=='(') goto err;
                if(x=='(') insideUserFun = true;
                if(!(insideUserFun || inmain) && x=='{') goto err;
                m.push(x);
            }
            else if(insideUserFun) instrcount++;
            if((insideUserFun && instrcount > 100) || anklecount > 1) goto err;
        }
        if(!m.empty() || anklecount!=1) goto err;
        cout << "No ";
        err: cout << "Compilation Errors\n" ;
    }
    return 0;
}
