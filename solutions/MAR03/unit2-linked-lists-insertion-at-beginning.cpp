#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;
typedef struct Node{
    int val;
    struct Node * next;
} Node ;
Node * insert(Node * head,int val){
    Node * znew = (Node *) malloc(sizeof(Node));
    znew->val = val;
    if(head)znew->next = head;
    else znew->next = NULL;
    if(!head) return znew;
    head = znew;
    return head;
}

void print(Node * head){
    if(!head) return ;
    if(head->next){
        printf("%d->",head->val);
    }
    else printf("%d\n",head->val);
    print(head->next);
}

int main() {
    int n;
    Node * head = NULL;
    cin >> n;
    while(n--){
        int x;
        cin >> x;
        head = insert(head,x);
    }
    print(head);
    return 0;
}
