#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    int n;
    cin >> n;
    if(n==1){
        cout << -1;
        return 0;
    }
    vector<int> v(n);
    int ans=1,k;
    for(int&x: v) cin >> x;
    for(int i=0;i<n;++i){
      for(int j=i+1;j<n;++j){
          if(v[i] && v[j] && (k=__gcd(v[i],v[j])) > ans)
              ans = k;
      }
    }
    cout << ans;
    return 0;
}
