#include <bits/stdc++.h>
using namespace std;

int solve(string&s ){
    stack<char> m;
    int n = s.length() /2;
    int i;
    for(i=0;i<n;++i) m.push(s[i]);
    if(s.length()&1) ++i;
    int ans = n;
    for(;i<s.length();++i){
        if(s[i]==m.top()) --ans;
        m.pop();
    }
    return ans;
}
int main() {
    int n;
    cin >> n;
    int ans = 0,zz;
    string ans_str;
    for(int i=0;i < n;++i){
        string s;
        cin >> s;
        if((zz = solve(s)) > ans)
            ans = zz,ans_str = s;
    }
    cout << ans_str << endl;
    return 0;
}
