#include <bits/stdc++.h>
using namespace std;

int main() {
    int t;
    cin >> t;
    while(t--){
       int n,id,g;
        char v;
        cin >> n >> id;
        stack<int> m;
        m.push(id);
        while(n--){
            cin >> v;
            if(v=='P') cin >> g,m.push(g);
            else{
                int j = m.top();
                m.pop();
                int k = m.top();
                m.push(j);
                m.push(k);
            }
        }
        cout << "Player " << m.top() << "\n";
    }
    return 0;
}
