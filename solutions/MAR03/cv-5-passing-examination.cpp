#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

int main() {
    int t;
    scanf("%d",&t);
    int q,s;
    while(t--){
       scanf("%d %d",&q,&s);
        if(q * 4 < s){
            printf("-1\n");
            continue;
        }
       else printf("%.2f\n",100.0 * (double)(3.0+(double)s/q)/7.0);
    }
    return 0;
}
