#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#define new znew
#define LinkedListNode Node
#define delete deleter
using namespace std;
typedef struct Node{
    int val;
    struct Node * next;
} Node ;
LinkedListNode* insert(LinkedListNode* head, int val) {
    LinkedListNode * new = (LinkedListNode *) malloc(sizeof(LinkedListNode));
    new->next = NULL;
    new->val = val;
    if(!head) return new;
    LinkedListNode * ref = head;
    while(head->next){
        head = head->next;
    }
    head->next = new;
    return ref;
}
LinkedListNode* deleter(LinkedListNode* head) {
    if(!head) return head;
    LinkedListNode * ref = head;
    head = head->next;
    free(ref);
    return head;
}


bool find(Node * head, int k){
    if(!head) return false;
    while(head){
        if(head->val == k) return true;
        head = head->next;
    }
    return false;
}

int main() {
    int n;
    Node * head = NULL;
    cin >> n;
    while(n--){
        int x,pos;
        cin >> x;
        head = insert(head,x);
    }
    int k;
    cin >> k;
    if(find(head,k)) cout << "yes";
    else cout << "no";
    return 0;
}
