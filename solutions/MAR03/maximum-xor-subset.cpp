#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    int n;
    cin >> n;
    vector<int> v(n);
    for(int&x: v) cin>> x;
    int ans= 0;
    for(int g=1;g < (1 << n); ++g){
        int temp=0;
        for(int j=0;j<n;++j){
            if(g&(1<<j)) temp^=v[j];
        }
        if(temp > ans)
            ans = temp;
    }
    cout << ans;
    return 0;
}
