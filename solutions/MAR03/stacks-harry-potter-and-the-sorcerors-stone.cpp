#include <bits/stdc++.h>
using namespace std;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int n,q,x;
    cin >> n;
    vector<int> v(n);
    for(auto&x: v) cin >> x;
    cin >> q >> x;
    int i=0,csum = 0;
    stack<int> monk;
    bool found = false;
    int ans;
    while(q--){
        string s;
        cin >> s;
        if(s=="Harry"){
            monk.push(v[i++]);
            csum += monk.top();
            if(csum == x && !found){
                ans = monk.size();
                found = true;
            }
        }
        else if(!monk.empty()){
            csum -= monk.top();
            monk.pop();
        }
    }
    if(found) cout << ans << endl;
    else cout << -1 << endl;
    return 0;
}
