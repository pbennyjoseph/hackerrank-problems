#include <bits/stdc++.h>
using namespace std;

int main() {
    string s;
    ostringstream os;
    cin >> s ;
    stack<char> m;
    int uu[255];
    uu['+']=1;
    uu['-']=1;
    uu['*']=2;
    uu['/']=2;
    for(char &x: s){
        if(isalpha(x)) os << x;
        else if (x=='(') m.push(x);
        else if(x==')'){
            while(m.top()!='(') os << m.top(), m.pop();
            m.pop();
        }
        else{
            while(!m.empty() && uu[m.top()] >= uu[x]) os << m.top(), m.pop();
            m.push(x);
        }
    }
    while(!m.empty()) os << m.top(),m.pop();
    cout << os.str() << endl;
    return 0;
    
}
