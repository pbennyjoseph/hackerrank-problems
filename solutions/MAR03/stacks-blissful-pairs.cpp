#include <bits/stdc++.h>
using namespace std;
using ll = long long;
int main() {
    int n;
    cin >> n;
    vector<int> v(n),rmx(n),lmx(n);
    for(int&x: v) cin >> x;
    // RMX AND LMX implementation w/o stack O(N)
    // P Benny Joseph
    rmx[n-1] = -1;
    lmx[0] = -1;
    for(int i=n-2;i>=0;--i){
        int k = i+1;
        while(k!=-1 && v[i] >= v[k])
            k = rmx[k];
        rmx[i] = k;            
    }
    for(int i=1;i<n;++i){
        int k = i-1;
        while(k!=-1 && v[i] >= v[k])
            k = lmx[k];
        lmx[i] = k;
    }
    int ans = 0;
    for(int i=0;i<n;++i){
        if(rmx[i]!=-1) ++ans;
        if(lmx[i]!=-1) ++ans;
    }
    cout << ans << endl;
    return 0;
}
