#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include <stdbool.h>
typedef struct LinkedListNode LinkedListNode;

struct LinkedListNode {
    int val;
    LinkedListNode *next;
};

LinkedListNode* _insert_node_into_singlylinkedlist(LinkedListNode *head, LinkedListNode *tail, int val) {
    if(head == NULL) {
        head = (LinkedListNode *) (malloc(sizeof(LinkedListNode)));
        head->val = val;
        head->next = NULL;
        tail = head;
    }
    else {
        LinkedListNode *node = (LinkedListNode *) (malloc(sizeof(LinkedListNode)));
        node->val = val;
        node->next = NULL;
        tail->next = node;
        tail = tail->next;
    }
    return tail;
}


/*
 * Complete the function below.
 */
/*
For your reference:
LinkedListNode {
    int val;
    LinkedListNode *next;
};
*/
#define Node LinkedListNode
LinkedListNode* reverseSublist(LinkedListNode* head, int n, int m) {
    if(!head) return head;
    Node * ref = head;
    Node * prev = NULL,*nt = NULL;
    int k = 0;
    while(--n && ref){
        prev = ref;
        ref = ref->next;
        ++k;
    }
    if(n) return head;
    Node * start_rep =ref;
    int arr[100001];
    int i=0;
    while(k < m && ref){
        arr[i++] = ref->val;
        ref = ref->next;
        ++k;
    }
    while(i && start_rep){
        start_rep->val = arr[--i];
        start_rep = start_rep->next ;
    }
    return head;
}

int main()
{
    FILE *f = stdout;
    char *output_path = getenv("OUTPUT_PATH");
    if (output_path) {
        f = fopen(output_path, "w");
    }

    LinkedListNode* res;
    int head_size = 0;

    LinkedListNode* head = NULL;
    LinkedListNode* head_tail = NULL;

    scanf("%d\n", &head_size);
    for(int i = 0; i < head_size; i++) {
        int head_item;
        scanf("%d", &head_item);
        head_tail = _insert_node_into_singlylinkedlist(head, head_tail, head_item);

        if(i == 0) {
            head = head_tail;
        }
    }


    int n;
    scanf("%d", &n);

    int m;
    scanf("%d", &m);

    res = reverseSublist(head, n, m);
    while (res != NULL) {
        fprintf(f, "%d ", res->val);

        res = res->next;
    }

    fclose(f);
    return 0;
}
