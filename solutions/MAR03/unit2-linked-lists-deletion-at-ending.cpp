#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#define new znew
#define LinkedListNode Node
#define delete deleter
using namespace std;
typedef struct Node{
    int val;
    struct Node * next;
} Node ;
LinkedListNode* insert(LinkedListNode* head, int val) {
    LinkedListNode * new = (LinkedListNode *) malloc(sizeof(LinkedListNode));
    new->next = NULL;
    new->val = val;
    if(!head) return new;
    LinkedListNode * ref = head;
    while(head->next){
        head = head->next;
    }
    head->next = new;
    return ref;
}
LinkedListNode* deleter(LinkedListNode* head) {
    if(!head) return head;
    if(!head->next) return NULL;
    LinkedListNode* ref = head;
    while(head->next && head->next->next)
        head = head->next;
    LinkedListNode* temp = head->next;
    head->next = NULL;
    free(temp);
    return ref;
}


void print(Node * head){
    if(!head) return ;
    if(head->next){
        printf("%d->",head->val);
    }
    else printf("%d\n",head->val);
    print(head->next);
}

int main() {
    int n;
    Node * head = NULL;
    cin >> n;
    while(n--){
        int x,pos;
        cin >> x;
        head = insert(head,x);
    }
    int k;
    cin >> k;
    for(int i=0;i<k;++i) head = delete(head);
    print(head);
    return 0;
}
