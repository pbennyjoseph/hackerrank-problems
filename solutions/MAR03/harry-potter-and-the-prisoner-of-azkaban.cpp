#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include <stdbool.h>
typedef struct LinkedListNode LinkedListNode;

struct LinkedListNode {
    int val;
    LinkedListNode *next;
};

LinkedListNode* _insert_node_into_singlylinkedlist(LinkedListNode *head, LinkedListNode *tail, int val) {
    if(head == NULL) {
        head = (LinkedListNode *) (malloc(sizeof(LinkedListNode)));
        head->val = val;
        head->next = NULL;
        tail = head;
    }
    else {
        LinkedListNode *node = (LinkedListNode *) (malloc(sizeof(LinkedListNode)));
        node->val = val;
        node->next = NULL;
        tail->next = node;
        tail = tail->next;
    }
    return tail;
}


/*
 * Complete the function below.
 */
/*
For your reference:
LinkedListNode {
    int val;
    LinkedListNode *next;
};
*/
#define Node LinkedListNode

Node * rev(Node *head){
    Node * prev = NULL,* nt = NULL;
    if(!head) return head;
    Node * ref = head;
    while(ref){
        nt = ref->next;
        ref->next = prev;
        prev = ref;
        ref = nt;
    }
    return prev;
}
LinkedListNode* addLists(LinkedListNode* list1, LinkedListNode* list2) {
    if(!list1) return list2;
    if(!list2) return list1;
    int i=0,carry = 0,g;
    Node * ref = rev(list1),*ng= rev(list2);
    int arr[100001];
    while(ref && ng){
        g = ref->val + ng->val + carry;
        carry = g/10;
        arr[i++] = g%10;
        ref = ref->next;
        ng = ng->next;
    }
    while(ref){
        g = ref->val + carry;
        carry = g/10;
        arr[i++] = g%10;
        ref = ref->next;
    }
    while(ng){
        g = ng->val + carry;
        carry = g/10;
        arr[i++] = g%10;
        ng = ng->next;
    }
    if(carry) arr[i++] = carry;
    Node * head = NULL,*curr=NULL;
    int j=0;
    while(j < i){
        Node * x = (Node *) malloc(sizeof(Node));
        x->val = arr[j++];
        x->next = NULL;
        if(!head) head = x,curr = x;
        else{
            curr->next = x;
            curr = curr->next;
        }
    }
    return rev(head);
}

int main()
{
    FILE *f = stdout;
    char *output_path = getenv("OUTPUT_PATH");
    if (output_path) {
        f = fopen(output_path, "w");
    }

    LinkedListNode* res;
    int list1_size = 0;

    LinkedListNode* list1 = NULL;
    LinkedListNode* list1_tail = NULL;

    scanf("%d\n", &list1_size);
    for(int i = 0; i < list1_size; i++) {
        int list1_item;
        scanf("%d", &list1_item);
        list1_tail = _insert_node_into_singlylinkedlist(list1, list1_tail, list1_item);

        if(i == 0) {
            list1 = list1_tail;
        }
    }


    int list2_size = 0;

    LinkedListNode* list2 = NULL;
    LinkedListNode* list2_tail = NULL;

    scanf("%d\n", &list2_size);
    for(int i = 0; i < list2_size; i++) {
        int list2_item;
        scanf("%d", &list2_item);
        list2_tail = _insert_node_into_singlylinkedlist(list2, list2_tail, list2_item);

        if(i == 0) {
            list2 = list2_tail;
        }
    }


    res = addLists(list1, list2);
    while (res != NULL) {
        fprintf(f, "%d", res->val);

        res = res->next;
    }

    fclose(f);
    return 0;
}
