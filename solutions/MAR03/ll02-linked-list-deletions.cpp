//-------------------- head of the code ------------------------

#include <string.h>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#define in(x) scanf(" %d", &x);
#define LinkedListNode LinkedListNode
typedef struct LinkedListNode LinkedListNode;
struct LinkedListNode {
	int val;
	struct LinkedListNode* next;
};


//-------------------- body of the code ------------------------


LinkedListNode* insertAtBeginning(LinkedListNode* head, int val) {
    LinkedListNode * new = (LinkedListNode *) malloc(sizeof(LinkedListNode));
    new->val = val;
    if(head)new->next = head;
    else new->next = NULL;
    if(!head) return new;
    head = new;
    return head;
}

LinkedListNode* insertAtEnd(LinkedListNode* head, int val) {
    LinkedListNode * new = (LinkedListNode *) malloc(sizeof(LinkedListNode));
    new->next = NULL;
    new->val = val;
    if(!head) return new;
    LinkedListNode * ref = head;
    while(head->next){
        head = head->next;
    }
    head->next = new;
    return ref;
}

LinkedListNode* insertAtPosition(LinkedListNode* head, int val, int pos) {
    LinkedListNode * new = (LinkedListNode *) malloc(sizeof(LinkedListNode));
    new->next = NULL;
    new->val = val;
    if(!head) return new;
    if(pos==1){
        new->next = head;
        return new;
    }
    LinkedListNode * ref = head;
    --pos;
    while(--pos && ref->next){
        ref = ref->next;
    }
    if(pos) return head;
    new->next = ref->next;
    ref->next = new;
    return head;
}
LinkedListNode* deleteAtBeginning(LinkedListNode* head) {
    if(!head) return head;
    LinkedListNode * ref = head;
    head = head->next;
    free(ref);
    return head;
}
LinkedListNode* deleteAtEnd(LinkedListNode* head) {
    if(!head) return head;
    if(!head->next) return NULL;
    LinkedListNode* ref = head;
    while(head->next && head->next->next)
        head = head->next;
    LinkedListNode* temp = head->next;
    head->next = NULL;
    free(temp);
    return ref;
}
LinkedListNode* deleteAtPosition(LinkedListNode* head, int pos) {
    if(!head) return head;
    if(!head->next) return NULL;
    LinkedListNode * ref = head;
    if(pos==1){
        head = head->next;
        return head;
    }
    --pos;
    while(--pos && head->next && head->next->next)
        head = head->next;
    LinkedListNode * temp = head->next;
    if(pos) return ref;
    head->next = head->next->next;
    return ref;
}


//-------------------- tail of the code ------------------------


int rng(int lim) {
	if (lim == 0) return 0;
	return rand()%lim;
}
int a[1005], sz = 0;
void insertt(int val, int pos) {
	if (pos < 0) return;
	if (pos > sz + 1) return;
	sz += 1;
	for (int i = sz; i > pos; i--)
		a[i] = a[i - 1];
	a[pos] = val;
}
void erasee(int pos) {
	if (pos > sz) return;
	if (pos < 1) return;
	sz--;
	for (int i = pos; i <= sz; i++)
		a[i] = a[i + 1];
}
int check(LinkedListNode* head) {
	for (int i = 1; i <= sz; i++) {
		if (head == NULL) return 0;
		if (head->val != a[i]) return 0;
		head = head->next;
	}	
	if (head != NULL) return 0;
	return 1;
}
int main() {
	srand(time(NULL));
	int t, n; in(t); in(n);
	while (t--) {
		LinkedListNode* head = NULL;
		sz = 0;
		for (int i = 0; i < n; i++) {
			int type = rng(6);
			if (type == 0) {
				int val = rng(1000);
				head = insertAtBeginning(head, val);
				insertt(val, 1);
				if (!check(head)) {
					printf("incorrect insertAtBeginning");
					return 0;
				}
			}

			if (type == 1) {
				int val = rng(1000);
				head = insertAtEnd(head, val);
				insertt(val, sz + 1);
				if (!check(head)) {
					printf("incorrect insertAtEnd");
					return 0;
				}
			}

			if (type == 2) {
				int val = rng(1000);
				int pos = rng(sz + 1) + 1;
				head = insertAtPosition(head, val, pos);
				insertt(val, pos);
				if (!check(head)) {
					printf("incorrect insertAtPosition");
					return 0;
				}
			}

			if (type == 3) {
				head = deleteAtBeginning(head);
				erasee(1);
				if (!check(head)) {
					printf("incorrect deleteAtBeginning");
					return 0;
				}
			}

			if (type == 4) {
				head = deleteAtEnd(head);
				erasee(sz);
				if (!check(head)) {
					printf("incorrect deleteAtEnd");
					return 0;
				}
			}

			if (type == 5) {
				int pos = rng(sz)+1;
				head = deleteAtPosition(head, pos);
				erasee(pos);
				if (!check(head)) {
					printf("incorrect deleteAtPosition");
					return 0;
				}
			}
		}
	}
	printf("correct");
}