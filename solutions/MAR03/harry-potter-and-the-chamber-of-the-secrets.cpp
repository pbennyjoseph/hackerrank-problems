#include <bits/stdc++.h>

using namespace std;
class LinkedListNode {
public:
    int val;
    LinkedListNode *next;

    LinkedListNode(int node_value) {
        val = node_value;
        next = NULL;
    }
};

LinkedListNode* _insert_node_into_singlylinkedlist(LinkedListNode *head, LinkedListNode *tail, int val) {
    if(head == NULL) {
        head = new LinkedListNode(val);
        tail = head;
    }
    else {
        LinkedListNode *node = new LinkedListNode(val);
        tail->next = node;
        tail = tail->next;
    }
    return tail;
}


/*
 * Complete the function below.
 */
/*
For your reference:
LinkedListNode {
    int val;
    LinkedListNode *next;
};
*/
#define Node LinkedListNode
bool hasCycle(LinkedListNode* head) {
   if(!head) return false;
    set<Node * > m;
    while(head){
        if(m.find(head)!=m.end()) return true;
        m.insert(head);
        head=head->next;
    }
    return false;
}

int main()
{
    ofstream fout(getenv("OUTPUT_PATH"));

    bool res;
    int head_size = 0;

    LinkedListNode* head = NULL;
    LinkedListNode* head_tail = NULL;

    cin >> head_size;

    for(int i = 0; i < head_size; i++) {
        int head_item;
        cin >> head_item;
        cin.ignore(numeric_limits<streamsize>::max(), '\n');
        head_tail = _insert_node_into_singlylinkedlist(head, head_tail, head_item);

        if(i == 0) {
            head = head_tail;
        }
    }

	int x;
  	cin >> x;
  	if (x > -1)
    {
      	LinkedListNode* ptr = head;
      	while (x--) ptr = ptr->next;
      	head_tail->next = ptr;
    }
  
    res = hasCycle(head);
    fout << res << endl;

    fout.close();
    return 0;
}
