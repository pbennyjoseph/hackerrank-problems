#include <bits/stdc++.h>
using namespace std;

inline int add(int i,int j){
    return i+j;
}

inline int mul(int i,int j){
    return i*j;
}

inline int sub(int i,int j){
    return i-j;
}

inline int _div(int i,int j){
    return i/j;
}

int main() {
    int g;
    string s;
    cin >> s;
    stack<int> m;
    
    int (*uu[])(int,int) = {add,mul,sub,_div};
    int zz[255];
    zz['+'] = 0;
    zz['*'] = 1;
    zz['-'] = 2;
    zz['/'] = 3;
    for(auto&x: s){
        if(isalpha(x)){
            cin >> g;
            m.push(g);
        }
        else{
            int i=m.top();
            m.pop();
            int j=m.top();
            m.pop();
            m.push(uu[zz[x]](j,i));
        }
    }
    cout << m.top() << endl;
    return 0;
}
