#include <bits/stdc++.h>
using namespace std;
using ll = long long;

/*
For each index i, compute nearest right and left min indices (say r,l);
The rectangle with height array[i] will span the range between r and l;
Compute maximum area of all these rectangles
*/
int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);
    int n;
    cin >> n;
    vector<ll> v(n);
    vector<int> rmx(n),lmx(n);
    for(ll&x: v) cin >> x;
    int k;
    
    // RMX AND LMX implementation w/o stack O(N)
    // P Benny Joseph
    rmx[n-1] = n;
    lmx[0] = -1;
    for(int i=n-2;i>=0;--i){
        int k = i+1;
        while(k!=n && v[i] <= v[k])
            k = rmx[k];
        rmx[i] = k;            
    }
    for(int i=1;i<n;++i){
        int k = i-1;
        while(k!=-1 && v[i] <= v[k])
            k = lmx[k];
        lmx[i] = k;
    }
    rmx[n-1] = n;
    // Debug
    
    // for(auto&x: v) cout << x << " ";
    // cout << endl;
    // for(auto&x: rmx) cout << x << " ";
    // cout << endl;
    // for(auto&x: lmx) cout << x << " ";
    // cout << endl;
    ll area = 0LL,zz;
    for(int i=0;i<n;++i){
        int x = rmx[i] - 1;
        int y = lmx[i] + 1;
        if((zz = v[i]*(x-y+1)) > area)
            area = zz;
    }
    cout << area << endl;
    return 0;
}
