#include <bits/stdc++.h>
using namespace std;


int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    cout.tie(NULL);
    int n,x,y;
    cin >> n;
    stack<int> m;
    stack<pair<int,int>> mix;
    while(n--){
        string s;
        cin >> s;
        if(s== "Add"){
           cin >> x;
            m.push(x);
            if(!mix.empty()) mix.push({max(mix.top().first,x),min(mix.top().second,x)});
            else mix.push({x,x});
         }
        else if(mix.empty()) goto inv;
        else if(s =="Remove") m.pop(),mix.pop();
        else if(s== "CallMin") cout << mix.top().second << "\n";
        else  cout << mix.top().first << "\n";
        continue;
        inv:cout << "Invalid\n";
    }
    return 0;
}
