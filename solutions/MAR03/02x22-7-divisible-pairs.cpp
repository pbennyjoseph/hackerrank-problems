#include <bits/stdc++.h>
using namespace std;


int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int n,x;
    cin >> n;
    int a[7];
    memset(a,0,sizeof(a));
    for(int i=0;i < n;++i){
        cin >> x;
        a[x%7]++;
    }
    long long ans = a[0]*(a[0]-1)/2;
    for(int i=1;i<4;++i){
        ans += a[i]*a[7-i];
    }
    cout << ans << endl;
    return 0;
}
