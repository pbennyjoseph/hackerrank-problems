#include <bits/stdc++.h>
using namespace std;


int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int n,v,x;
    cin >> n;
    stack<int> m,mx;
    while(n--){
        cin >> v;
        switch(v){
            case 1: cin >> x;
                    m.push(x);
                    if(!mx.empty() && mx.top() > x) x= mx.top();
                    mx.push(x);
                    break;
            case 2: m.pop(),mx.pop();
                    break;
            case 3:  cout << mx.top() << "\n";
                    break;
        }
    }
    return 0;
}
