/*
Name: P Benny Joseph
Date: 08-Feb-2020
*/
#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

int main() {
    int t,n;
    cin >> n;
    vector<int> v(n);
    for(int&x: v) cin >> x;
    int a=0,b=0,turn =0;
    int start=0,end=n-1;
    for(int i=0;i<n;++i){
        if(turn){
            if(v[start] > v[end]){
                b += v[start];
                start++;
            }
            else{
                b += v[end];
                end--;
            }
        }
        else{
            if(v[start] > v[end]){
                a += v[start];
                start++;
            }
            else{
                a += v[end];
                end--;
            }
        } 
        turn ^= 1;
    }
    cout << a << " " << b << endl;
    return 0;
}
