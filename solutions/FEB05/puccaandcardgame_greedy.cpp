/*
Name: P Benny Joseph
Date: 08-Feb-2020
*/
#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

int main() {
    int n;
    cin >> n;
    vector<int> v(n);
    for(int &x: v) cin >> x;
    int i=0,j=n-1,who=0;
    long long p=0LL,g = 0LL;
    for(int _=0;_<n;_++){
        if(!who){
            if(v[i] >= v[j])
                p += v[i++];
            else p += v[j--];
        }
        else{
            if(v[i] >= v[j])
                g += v[i++];
            else g += v[j--];
        }
        who ^= 1;
    }
    if(p < g) cout << "Garu\n";
    else cout << "Pucca\n";
    return 0;
}
