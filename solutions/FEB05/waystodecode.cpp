/*
Name: P Benny Joseph
Date: 08-Feb-2020
*/
#include <bits/stdc++.h>
using namespace std;
using ll = long long;
const int MOD = 1e9+7LL;

inline ll add(ll a,ll b){
    return ((a%MOD)+(b%MOD))%MOD;
}
int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    string s;
    cin >> s;
    int n = s.length();
    vector<ll> ans(n,0);
    
    ans[0] = 1;
    if(n > 1) ans[1] = 1+(s.substr(0,2) < "25");
    // at every index i, if feasible add to A[i-2] otherwise 
    // interpret it as only 1 and add A[i-1]
    for(int i=2;i<n;++i){
        // cout << s.substr(i-1,i) << endl;
        if(s[i-1] < '2' || (s[i-1]=='2' && s[i] <= '5'))
            ans[i] = add(ans[i],ans[i-2]);
        ans[i] = add(ans[i],ans[i-1]);
        // cout << ans[i] << endl;
    }
    // for(ll&x: ans) cout << x << endl;
    cout << ans[n-1] << endl;
    return 0;
}