/*
Name: P Benny Joseph
Date: 08-Feb-2020
*/
#include <bits/stdc++.h>
using namespace std;

long long havecoins = 0LL;

int main() {
    int n,m,t;
    map<int,int> u;
    cin >> n >> m;
    for(int i=0;i<n;++i){
        cin >> t;
        havecoins += t;
        u[t]++;
    }
    vector<pair<int,int>> v;
    for(auto it = u.begin();it!=u.end();++it){
        v.push_back({it->first,it->second});
    }
    sort(v.begin(),v.end());
    int it=0;
    while(m--){
        cin >> t;
        int value = 0,coins = 0;
        if(t > havecoins) goto nope;
        
        while(value < t){
            if(v[it].second ==0 ) ++it;
            value += v[it].first;
            havecoins -= v[it].first;
            v[it].second--;
            coins++;
        }
        cout << coins << " " << value << "\n";
        continue;
        nope: cout << "-1 -1" << '\n';
    }
    return 0;
}
