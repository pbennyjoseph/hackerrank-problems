/*
Name: P Benny Joseph
Date: 08-Feb-2020
*/
#include <iostream>
#include <vector>
 
using namespace std;
 
int main(){
    int size,n,t;
    cin  >> t;
    while(t--){
        cin >> n >> size;
        int sizes[n],value[n];
        vector<vector<int>> v(2,vector<int> (size + 1,0));
        for(int i=0;i <n ; ++i){
            cin >> sizes[i] >> value[i];
        }
        int prev = 0;
        for(int i=0; i < n; ++i){
            for(int j=0;j < min(sizes[i],size+1); ++j)
                v[!prev][j] = v[prev][j];
            for(int j = sizes[i]; j <= size; ++j){
                v[!prev][j] = max(v[prev][j], value[i] + v[prev][j - sizes[i]]);
            }
            prev ^= 1;
        }
        cout << v[prev][size] << endl;
    }
    return 0;
}
 