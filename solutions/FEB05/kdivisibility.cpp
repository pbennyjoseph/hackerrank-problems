/*
Name: P Benny Joseph
Date: 08-Feb-2020
*/
#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    int n,k;
    cin >> n >> k;
    vector<long long> v(k,0);
    int t;
    for(int i=0;i<n;++i){
        cin >> t;
        v[t%k]++;
    }
    long long ans = v[0]*(v[0]-1)/2;
    for(int i=1;i<(k+1)/2;++i){
        ans += v[i]*v[k-i];
    }
    if(k%2==0) ans += v[k/2]*(v[k/2]-1)/2;
    cout << ans << endl;
    return 0;
}
