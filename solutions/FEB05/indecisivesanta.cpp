/*
Name: P Benny Joseph
Date: 08-Feb-2020
*/
#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

inline int dist(int x1,int y1,int x2,int y2){
    return abs(x1-x2) + abs(y1-y2);
}

int main() {
    int n,m;
    cin >> n >>m;
    int a,b,x,y;
    cin >> a >> b >> x >> y;
    int ans = 0;
    for(int i=1;i<=n;++i){
        for(int j=1;j<=m;++j){
            if(i==x && j==y) continue;
            if(i==a && j==b) continue;
            if(dist(a,b,i,j)==dist(x,y,i,j)) ++ans;
        }
    }
    cout << ans << endl;
    return 0;
}
