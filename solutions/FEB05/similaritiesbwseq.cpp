/*
Name: P Benny Joseph
Date: 08-Feb-2020
*/
#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    int n,m,t;
    cin >> n;
    int l1[n+1];
    for(int i=0;i<n;++i){
        cin >> l1[i];
    }
    cin >> m;
    int l2[m+1];
    for(int i=0;i<m;++i){
        cin >> l2[i];
    }
    int dp[m+1][n+1];
    int z= 0;
    for (int i = 0; i < m; i++) {
        for (int j = 0; j < n; j++) {
            if(l1[j]==l2[i]){
                if(i==0 || j==0)
                    dp[i][j]=1;
                else dp[i][j] = dp[i-1][j-1]+1;
                if(dp[i][j] > z)
                    z = dp[i][j];
            }
            else dp[i][j] = 0;
        }
    }
    cout << z << endl;
    return 0;
}
