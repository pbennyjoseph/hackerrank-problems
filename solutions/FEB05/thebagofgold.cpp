/*
Name: P Benny Joseph
Date: 08-Feb-2020
*/
#include <bits/stdc++.h>
using namespace std;
using ll = long long;
struct item{
    ll weight;
    ll value;
    double vw;
    item(ll w,ll v): weight(w),value(v){vw = (double) v/w;}
};

int main() {
    ll n,w,x,y;
    cin >> n >> w;
    vector<item> v;
    ll zsum = 0;
    for(int i=0;i<n;++i){
        cin >> x >> y;
        zsum += x;        
        v.push_back(item(x,y));
    }
    if(zsum < w){
        cout << -1 ;
        return 0;
    }
    sort(v.begin(),v.end(),[&](const item&lhs,const item&rhs){
        if(lhs.vw == rhs.vw){
            if(lhs.value == rhs.value)
                return lhs.weight < rhs.weight;
            return lhs.value > rhs.value;
        }
        return lhs.vw > rhs.vw;
    });
    ll i = 0,used = 0;
    double profit = 0;
    
    while(i < n && used + v[i].weight <= w){
        used += v[i].weight;
        profit += v[i].value;
        ++i;
    }
    if(i < n && used < w){
        profit +=  v[i].value * ((double)(w-used)/v[i].weight);
    }
    printf("%.012f",profit);
    return 0;
}
