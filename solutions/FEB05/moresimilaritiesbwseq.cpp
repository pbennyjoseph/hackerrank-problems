/*
Name: P Benny Joseph
Date: 08-Feb-2020
*/
#include <bits/stdc++.h>
using namespace std;

int main() {
    int n;
    cin >> n;
    vector<int> s(n);
    for(int&x: s) cin >> x;
    int m;
    cin >> m;
    vector<int> t(m);
    for(auto&x: t) cin >> x;
    long long dp[n+1][m+1];
    for(int i=0;i <= n;++i){
        for(int j=0;j<=m;++j){
            if(i==0 || j==0)
                dp[i][j] = 0;
            else if(s[i-1]==t[j-1])
                dp[i][j] = dp[i-1][j-1] + 1;
            else
                dp[i][j] = max(dp[i][j-1],dp[i-1][j]);
        }
    }
    cout << dp[n][m] << endl;
    return 0;
}