/*
Name: P Benny Joseph
Date: 08-Feb-2020
*/
#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    int n,m;
    cin >> n >>m;
    vector<vector<long long>> v(n,vector<long long>(m));
    for(auto&row : v){
        for(auto& x : row)
            cin >> x;
    }
    
    auto valid = [&](const int &i ,const int&j){
        return i >=0 && i < n && j >=0 && j < m;
    };
    for(int i=n-1;i>=0;--i){
        for(int j=m-1;j>=0;--j){
            if(valid(i,j+1) && valid(i+1,j))
                v[i][j] += min(v[i][j+1],v[i+1][j]);
            else if(valid(i,j+1))
                v[i][j] += v[i][j+1];
            else if(valid(i+1,j))
                v[i][j] += v[i+1][j];
        }
    }
    cout << v[0][0] << endl; 
    return 0;
}
