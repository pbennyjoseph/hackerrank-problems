/*
Name: P Benny Joseph
Date: 08-Feb-2020
*/
#include <bits/stdc++.h>
using namespace std;

int main() {
    using ll = long long;
    int n,t;
    cin >> n;
    int freq[8];
    memset(freq,0,sizeof(freq));
    for(int i=0;i<n;++i){
        cin >> t;
        freq[t%7]++;
    }
    ll ans = freq[0]*(freq[0]-1)/2;
    for(int i=1;i<4;++i){
        ans += freq[i]*freq[7-i];
    }
    cout << ans << endl;
    return 0;
}