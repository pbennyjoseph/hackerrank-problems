/*
Name: P Benny Joseph
Date: 08-Feb-2020
*/
#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

int main() {
    int n;
    cin >> n;
    int rows = 2*n-1;
    for(int i=0;i<rows;++i){
        for(int j=0;j<rows;++j){
            int k = rows/2;
            if(j >= k - min(i,rows-i-1)  && j <= k + min(i,rows - i-1) )cout << "*";
            else cout << " ";
        }
        cout << endl;
    }
    return 0;
}
