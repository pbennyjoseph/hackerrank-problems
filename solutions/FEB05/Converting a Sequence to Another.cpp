/*
Name: P Benny Joseph
Date: 08-Feb-2020
*/
#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    int n,m,t;
    cin >> n;
    int l1[n+1];
    for(int i=0;i<n;++i){
        cin >> l1[i];
    }
    cin >> m;
    int l2[m+1];
    for(int i=0;i<m;++i){
        cin >> l2[i];
    }
    int dp[m+1][n+1];
    for (int i = 0; i <= m; i++) {
        dp[i][0] = i;
    }
    for (int j = 0; j <= n; j++) {
        dp[0][j] = j;
    }

    for (int i = 1; i <= m; i++) {
        for (int j = 1; j <= n; j++) {
            if (l1[j-1] == l2[i-1]) dp[i][j] = dp[i-1][j-1];
            else dp[i][j] = 1 + min(min(dp[i][j-1],dp[i-1][j]),dp[i-1][j-1]);
        }
    }
    cout << dp[m][n] << endl;
    return 0;
}
