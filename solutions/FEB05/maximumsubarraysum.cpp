/*
Name: P Benny Joseph
Date: 08-Feb-2020
*/
#include <bits/stdc++.h>
using namespace std;

int main() {
    int n;
    cin >> n;
    vector<int> v(n);
    for(int&x : v) cin >> x;
    long long mx=INT_MIN,me=0;
    for(auto&x: v){
        me += x;
        if(me < 0) me=0;
        if(mx < me) mx = me;
    }
    cout << mx << endl;
    return 0;
}