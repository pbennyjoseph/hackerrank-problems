/*
Name: P Benny Joseph
Date: 08-Feb-2020
*/
#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    int n;
    cin >> n;
    int k=1;
    for(int i=0;i<n;++i){
        for(int j=0;j<n-i-1;++j) cout << " ";
        for(int j=0;j<=i;++j){
            printf("%05d ",8*(k*k)-2*k);
            ++k;
        }
        cout << endl;
    }
    return 0;
}