/*
Name: P Benny Joseph
Date: 08-Feb-2020
/*
Name: P Benny Joseph
Date: 08-Feb-2020
*/
#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

void prmsv(vector<int>& primes){
    primes.push_back(2);
    vector<bool> isprm(1000000,true);
    for(int i=3;i <= 1000000;i+= 2){
        if(isprm[i]){
            primes.push_back(i);
            for(long long j=3*i;j < 1000000;j += i)
                isprm[j] = false;
        }
    }
}
int main() {
    int n;
    vector<int> primes;
    prmsv(primes);
    cin >> n;
    int k=0;
    for(int i=0;i<n;++i){
        for(int j=0;j<=i;++j){
            cout << primes[k++] << " ";
        }
        cout << endl;
    }
    return 0;
}