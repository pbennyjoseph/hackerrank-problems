/*
Name: P Benny Joseph
Date: 08-Feb-2020
*/
#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    int n;
    cin >>n;
    int a[n];
    for(int i=0;i<n;++i){
        cin >> a[i];
    }
    int i = 1;
    while(a[i] < a[i-1] && i < n) ++i;
    if(i==1){
        cout << "no" ;
        return 0;
    }
    while(a[i] == a[i-1] && i <n) ++i;
    if(i==n){
        cout << "no" ;
        return 0;
    }
    while(a[i] > a[i-1] && i< n) ++i;
    if (i==n){
        cout << "yes";
    }
    else cout << "no";
    return 0;
}
