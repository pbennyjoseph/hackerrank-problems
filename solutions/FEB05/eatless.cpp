/*
Name: P Benny Joseph
Date: 08-Feb-2020
*/
#include <bits/stdc++.h>
using namespace std;
using ll = long long;
int main() {
    int n;
    cin >> n;
    vector<long long > v(n);
    for(auto&x: v) cin >> x;
    sort(v.begin(),v.end(),greater<int>());
    long long temp= 1LL,ans = 0LL;
    for(int i=0;i<n;++i){
        ans += (1LL << i)*v[i];
    }
    cout << ans << endl;
    return 0;
}
