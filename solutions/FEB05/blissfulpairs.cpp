/*
Name: P Benny Joseph
Date: 08-Feb-2020
*/
#include <bits/stdc++.h>
using namespace std;
typedef unsigned long long int ull;

int main(){
    int t,n;
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    cin >> n;
    vector<int> v(n);
    for(int&x: v) cin >> x;
    vector<int> rmx(n,-1), lmx(n,-1);
    for(int i=1;i<n;++i){
        int k = i-1;
        while(k!=-1 && v[k] <= v[i])
            k = lmx[k];
        lmx[i] = k;
    }
    for(int i=n-2; i>=0 ;--i){
        int k=i+1;
        while(k!=-1 && v[k] <= v[i])
            k = rmx[k];
        rmx[i] = k;
    }
    int ans = 0;
    for(int i=0;i<n;++i){
        if(rmx[i]!=-1)++ans;
    }
    for(int i=n-1;i>=0;--i){
        if(lmx[i]!=-1)++ans;
    }
    cout << ans << endl;
    return 0;
}