/*
Name: P Benny Joseph
Date: 08-Feb-2020
*/
#include <bits/stdc++.h>
using namespace std;

int lis(vector<int> const& a) {
    int n = a.size();
    const int INF = 1e9;
    vector<int> d(n+1, INF);
    d[0] = -INF;

    for (int i = 0; i < n; i++) {
        int j = upper_bound(d.begin(), d.end(), a[i]) - d.begin();
        if (d[j-1] < a[i] && a[i] < d[j])
            d[j] = a[i];
    }

    int ans = 0;
    for (int i = 0; i <= n; i++) {
        if (d[i] < INF)
            ans = i;
    }
    return ans;
}

int main() {
    int t,n;
    cin >> n;
    vector<int> v(n);
    for(int&x: v) cin >> x;
    reverse(v.begin(),v.end());
    cout << lis(v)-1 << endl;
    return 0;
}
