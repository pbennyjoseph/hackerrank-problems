/*
Name: P Benny Joseph
Date: 08-Feb-2020
*/
#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int ans(int n,int k){
    if(n==1) return n;
    return (ans(n-1,k) + k-1)%n+1;
}
int main() {
    int n,k;
    cin >> n >>k;
    cout << ans(n,k)  << endl;
    return 0;
}
