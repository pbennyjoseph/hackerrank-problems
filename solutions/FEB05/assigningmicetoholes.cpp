/*
Name: P Benny Joseph
Date: 08-Feb-2020
*/
#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    int n;
    cin >> n;
    vector<int> v(n),hole(n);
    for(int &x: v) cin >> x;
    for(int &x: hole) cin >> x;
    sort(v.begin(),v.end());
    sort(hole.begin(),hole.end());
    int ans = 0;
    for(int i=0;i<n;++i)
        ans = max(ans,abs(v[i]-hole[i]));
    cout << ans << endl;
    return 0;
}
