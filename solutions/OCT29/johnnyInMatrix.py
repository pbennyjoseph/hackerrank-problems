#Name: P Benny Joseph
#Date: 27-Oct-2019 10:31 AM
n = int(input())
l = []
for _ in range(n):
    l.append(list(map(int,input().split())))
row = [x for z in l for x in z]
col = [x for z in zip(*l) for x in z]
rowpr = sum([x*y for x,y in zip(row,row[1:])])
colpr = sum([x*y for x,y in zip(col,col[1:])])
print("column-major" if colpr < rowpr else "row-major")