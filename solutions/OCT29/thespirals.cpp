/*
Name: P Benny Joseph
Date: 27-Oct-2019 10:31 AM
*/
#include <bits/stdc++.h>
using namespace std;
int main(){
    int n;
    cin >> n;
    int r[2]={n-1,n-1},st = 1,sums[2]={-1,1},coord[2]={0,n-1};
    int a[n][n] = {0}; // Array to store the numbers
    int k = 1;	// Parameter for the number increments
	// First element is 1
    a[0][0] = k++;
	
	// Fill first row.
    for(int i=1;i<n;++i){
        a[0][i]=k++;
    }
    k = n+1;
	// do the stuff.
    while(1){
        if(r[(st+1)%2] <= 0) break;
        for(int _=0;_ < r[(st+1)%2];_++){           
            coord[(st+1)%2] += sums[(st)%4 < 2];
            a[coord[0]][coord[1]] = k++;
        }
        r[(st+1)%2]--;
        ++st;
    }
    
	// Print the array
    for(int i=0;i<n;++i){
      for(int j=0;j<n;++j){
        printf("%d ",a[i][j]);
      }
        cout << endl;
    }
    return 0;
}