/*
Name: P Benny Joseph
Date: 27-Oct-2019 10:31 AM
*/
#include <bits/stdc++.h>
using namespace std;

int solve(int n,int m,int var0,int var1){
    int start = min(var0,var1);
    int diff = (var0%n==start)?n:m;
    while(start < n*m && (start%n !=var0 || start%m !=var1)){
        start += diff;
    }
    return start;
}

int main(){
    int n,m,a0,a1,b0,b1,c0,c1,a,b,c,am,bm,cm,nmx;
    cin >> n >> m;
    cin >> a0 >> a1;
    cin >> b0 >> b1;
    cin >> c0 >> c1;
    // we need to solve nx-my = var0-var1
    // gcd of n,m = 1
    // find one solution.
    a = solve(n,m,a0,a1);
    b = solve(n,m,b0,b1);
    c = solve(n,m,c0,c1);
    cout << 2*a + b - c << endl;
    return 0;
}
// 23 29
// 7 1
// 2 25
// 4 21