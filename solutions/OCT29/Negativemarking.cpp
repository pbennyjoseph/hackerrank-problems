/*
Name: P Benny Joseph
Date: 27-Oct-2019 10:31 AM
*/
// Attempt 4
#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

// we need to minimize correct/total_attempted  (accuracy)
// on the constraint score > pass_score
// score = 4 * correct + (-3) * (wrong)
// correct + wrong  = total_attempted
// => score = 7*ca-3*att
// => ca/att > (3 + pass_score/att)/7
// for this to be minimum, maximise att, i.e. @code{a}

int main() {
    int t;
    int a ,b;
    cin >> t;
    while(t--){
        cin>> a >> b;
        if(a*4 < b){
            cout << -1 << endl;
            continue;
        }
        int ca = 1/7;	// Garbage.
        
        printf("%.2f\t", 100* (3.0+ (double) b/a)/7.0);
		printf("%g %d\n",(int) a*((3.0+ (double) b/a)/7.0),a);
    }
    return 0;
}
