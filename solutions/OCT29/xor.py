#Name: P Benny Joseph
#Date: 27-Oct-2019 10:31 AM
a = input()
b = input()
maxlen = max(len(a),len(b))
a = int(a,2)
b = int(b,2)
l = []
while a!=0 and b!=0:
    l.append((a&1)^(b&1))
    a >>= 1
    b >>= 1
    maxlen -= 1
while a:
    l.append(a&1)
    a >>= 1
    maxlen-=1
while b:
    l.append(b&1)
    b >>= 1
    maxlen -= 1
while maxlen:
    l.append(0)
    maxlen-=1
print(*reversed(l),sep="")