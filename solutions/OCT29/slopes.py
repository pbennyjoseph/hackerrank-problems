#Name: P Benny Joseph
#Date: 27-Oct-2019 10:31 AM
from math import tan
x1,y1,x2,y2 = map(int,input().split())
x3,y3,x4,y4 = map(int,input().split())
d1= y2-y1
d2 = x2-x1
d3 = y4-y3
d4 = x4-x3
j = False
try:
    j = tan(d1/d2)==tan(d3/d4)
except:
    if d2==0 and d4==0:
        j = True
print("yes" if j else "no")