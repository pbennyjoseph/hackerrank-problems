#Name: P Benny Joseph
#Date: 27-Oct-2019 10:31 AM
from math import ceil,sqrt
s = input()
j = ceil(sqrt(len(s)))
s += '?'*(j*j-len(s))
print(*[s[i:i+j] for i in range(0,j*j+1,j)],sep="\n")