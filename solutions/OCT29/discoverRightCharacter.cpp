/*
Name: P Benny Joseph
Date: 27-Oct-2019 10:31 AM
*/
#include <bits/stdc++.h>
using namespace std;


int main() {
    int n;
    char uu;
    cin >> n;
    stack<char> s;
    while (n > 0){
        int j = n%26;
        if(j==0){
            j = 26;
            n -= 26;
        }
        s.push('A'+j-1);
      
        n /= 26;
    }
    while(!s.empty()){
        cout << s.top() ;
        s.pop();
    }
    return 0;
}
