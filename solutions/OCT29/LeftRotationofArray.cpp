/*
Name: P Benny Joseph
Date: 27-Oct-2019 10:31 AM
*/
#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    int n,temp,d;
    cin >> n;
    vector<int> v;
    for(int i=0;i<n;++i){
        cin >> temp;
        v.push_back(temp);
    }
    cin >> d;
    rotate(v.begin(),v.begin()+v.size()-d%n,v.end());
    for(auto &x:v) cout << x << " ";
    return 0;
}
