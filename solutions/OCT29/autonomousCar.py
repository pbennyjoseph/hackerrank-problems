
#Name: P Benny Joseph
#Date: 27-Oct-2019 10:31 AM
t = int(input())
for _ in range(t):
    f,b,t,d = map(int,input().split())
    if (b > d):
        print(t*d)
        continue
    # we move in steps of b-f backward.
    # we cannot go further than b - (b-f)
    temp = d
    diff = b-f
    jmps = 0
    while temp > b:
        temp -= diff
        jmps += 1
    jmps *= (b+f)
    remaining = temp
    print(t*(jmps+remaining))