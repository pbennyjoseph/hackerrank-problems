/*
Name: P Benny Joseph
Date: 27-Oct-2019 10:31 AM
*/
#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    int n;
    cin >>n;
    int a[n];
    for(int i=0;i<n;++i){
        cin >> a[i];
    }
    int i = 1;
    while(a[i] > a[i-1] && i < n) ++i;
    while(a[i] == a[i-1] && i <n) ++i;
    while(a[i] < a[i-1] && i< n) ++i;
    if (i==n){
        cout << "yes";
    }
    else cout << "no";
    return 0;
}