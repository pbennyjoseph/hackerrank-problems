/*
Name: P Benny Joseph
Date: 27-Oct-2019 10:31 AM
*/
#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    int n,k;
    cin >> k;
    cin >> n;
    int a[n];
    for(int i=0;i<n;++i){
        cin >> a[i];
    }
    int count = 0;
    int i= 0;
    while(i < n){
        int zu = k;
        if(i < n && a[i] >= zu){
          cout << -1 ;
            return 0;
        }
        while(i < n && a[i] < zu){
           zu -= a[i];
            ++i;
        }
        ++count;
    }
    cout << count << endl;
    return 0;
}