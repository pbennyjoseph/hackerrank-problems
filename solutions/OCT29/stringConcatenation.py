#Name: P Benny Joseph
#Date: 27-Oct-2019 10:31 AM
from collections import Counter
p = int(input())
s = input()
g = Counter(s)
print("YES" if all([g[key]%p==0 for key in g]) else "NO")