/*
Name: P Benny Joseph
Date: 27-Oct-2019 10:31 AM
*/
#include <bits/stdc++.h>
using namespace std;

int main() {
    int n;
    cin >> n;
    int a[n],p[n],ans=0;
    // Reading 
    for(int i=0;i<n;++i){
        cin >> a[i] >> p[i];
    }
    // mix - Minimum element until now
	int mix = INT_MAX;
	
    for(int i=0;i<n;++i){
        mix =  min(mix,p[i]);
        ans += a[i]*mix;
    }
    cout << ans << endl;
    return 0;
}
