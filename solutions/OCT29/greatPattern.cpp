/*
Name: P Benny Joseph
Date: 27-Oct-2019 10:31 AM
*/
#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

void print(int n){
    int rows = n*2-1;
    for(int i=0;i<rows;++i){
        int k = 1;
        for(int j=0;j<rows;++j){
            if(abs(j-n+1) <= min(i,rows-i-1) ){
                int l = i,m=j;
                if(l > n-1) l = rows-l-1;
                if(m > n-1) m = rows-m-1;
                cout << l+m-n+2 << " ";
            }
            else cout << "  ";
        }
        cout << endl;
    }
    cout << endl;
}

int main() {
    int t,n;
    cin >> t;
    while(t--){
        cin >> n;
        print(n);
    }
    return 0;
}
