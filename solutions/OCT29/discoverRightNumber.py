#Name: P Benny Joseph
#Date: 27-Oct-2019 10:31 AM
s = input()[::-1]
sum = 0
pw = 1
for i in s:
    sum += (ord(i)-65+1)*pw
    pw *= 26
print(sum)