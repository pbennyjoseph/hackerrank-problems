#Name: P Benny Joseph
#Date: 27-Oct-2019 10:31 AM
def check(i,j):
    d = "aeiouy"
    if i==j:
        return False
    elif i in d and j in d:
        return False
    return True
s = input()
for i,j in zip(s,s[1:]):
    if not check(i,j):
        print("No")
        break
else:
    print("Yes")