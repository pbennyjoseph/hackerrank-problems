/*
Name: P Benny Joseph
Date: 27-Oct-2019 10:31 AM
*/
#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

int main() {
    int n;
    cin >> n;
    int a[11][11];
    int b[11][11];
	
	//Read
    for(int i=0;i<n;++i){
        for(int j=0;j<n;++j){
            cin >> a[i][j];
        }
    }
	
	//Rotate into another one
    for(int i=0;i<n;++i){
        for(int j=0;j<n;++j){
           b[j][n-i-1] = a[i][j];
        }
    }
    
	// Print
    for(int i=0;i<n;++i){
        for(int j=0;j<n;++j){
           cout << b[i][j] << " ";
        }
        cout << endl;
    }
    
    return 0;
}
