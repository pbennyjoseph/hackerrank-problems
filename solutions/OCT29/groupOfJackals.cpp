/*
Name: P Benny Joseph
Date: 27-Oct-2019 10:31 AM
*/
#include <bits/stdc++.h>
using namespace std;


int main() {
    int n,mx=INT32_MIN,sum=0;
    cin >> n;
    int a[n];
    for (int i=0;i<n;++i){
      cin >> a[i];
    }
    sum += a[n-1];
    mx = a[n-1];
    for(int i=n-2;i>=0;--i){
        
        if(a[i] > mx) sum += a[i];
        mx = max(a[i],mx);
    }
    cout << sum << endl;
    return 0;
}
