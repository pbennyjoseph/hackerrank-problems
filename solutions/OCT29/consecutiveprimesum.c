/*
Name: P Benny Joseph
Date: 27-Oct-2019 10:31 AM
*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define LIMIT 1000000

long long mulmod(long long a, long long b, long long mod)
{
    long long x = 0,y = a % mod;
    while (b > 0)
    {
        if (b % 2 == 1)
        {    
            x = (x + y) % mod;
        }
        y = (y * 2) % mod;
        b /= 2;
    }
    return x % mod;
}
long long modulo(long long base, long long exponent, long long mod)
{
    long long x = 1;
    long long y = base;
    while (exponent > 0)
    {
        if (exponent % 2 == 1)
            x = (x * y) % mod;
        y = (y * y) % mod;
        exponent = exponent / 2;
    }
    return x % mod;
}
int checkPrime(long long p)
{
    int i;
    long long int as[5] = {2,3,5,7,11};
    //long long int as[4] = {2,13,23,1662803};
    long long s;
    if (p < 2)
    {
        return 0;
    }
    if (p != 2 && p % 2==0)
    {
        return 0;
    }
     s = p - 1;
    while (s % 2 == 0)
    {
        s /= 2;
    }
    for (i = 0; i < 5; i++)
    {
        long long a = as[i], temp = s;
        long long mod = modulo(a, temp, p);
        while (temp != p - 1 && mod != 1 && mod != p - 1)
        {
            mod = mulmod(mod, mod, p);
            temp *= 2;
        }
        if (mod != p - 1 && temp % 2 == 0)
        {
            return 0;
        }
    }
    return 1;
}

int main(int argc, char const *argv[])
{    
    int i,j,pr_count=1,count=0;
    long long int sum=0,N=0;
    //printf("Enter Higher limit\n");
    scanf("%lld",&N);
    int* l = (int *) calloc(LIMIT,sizeof(int));
    int* primes  = (int *) calloc(LIMIT,sizeof(int));
    primes[0] = 2;
    for(i=3;i<LIMIT;i+=2)
    {
        if (!l[i])
        {
            for(j=2*i;j<LIMIT;j+=i)
                l[j] = 1;
            primes[pr_count++] = i;
        }
    }
    i = 0;
    while (sum < N)
    {
        if(sum > LIMIT && sum%2)
        {
            if (checkPrime(sum))
            {
                count++;
            }
        }
        else {
            if (sum%2 && !l[sum])
            {
                count++;
            }
        }
        sum += primes[i];
        i++;
    }
    printf("%d\n",count);
    return 0;
}